import logo from "./logo.svg";
import "./App.css";
import React from "react";
import arr from "./components/Totaldata";
import searchByletter from "./components/Searchdata";
import got from "./data";
// import familyByName from "./components/familyByName";

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      people: arr,
      activeButton: "",
    };
  }

  inputEventHandler = (letter) => {
    this.setState({
      people: searchByletter(letter),
      activeButton: "",
    });
  };

  clickHandler = (event) => {
    let name = event.target.value;
    got.houses.map((el) => {
      if (el.name === name) {
        this.setState({
          people: el.people,
          activeButton: name,
        });
      }
    });
  };

  render() {
    const { activeButton } = this.state;

    return (
      <>
        <header className="header">
          <h1>People of Got</h1>
          <input onChange={(e) => this.inputEventHandler(e.target.value)} />
        </header>
        <div className="family-buttons">
          {got.houses.map((item) => {
            const activeButtonClass =
              activeButton === item.name ? "active-button" : "buttons";
            return (
              <button
                value={item.name}
                onClick={this.clickHandler}
                className={activeButtonClass}
              >
                {item.name}
              </button>
            );
          })}
        </div>
        <main className="main-section">
          {this.state.people.map((famObj) => {
            return (
              <div className="per-person">
                <img src={famObj.image} />
                <p>{famObj.name}</p>
                <p>{famObj.description}</p>
                <button className="know-more-button">
                  <a href={famObj.wikilink}>Know More!</a>
                </button>
              </div>
            );
          })}
        </main>
      </>
    );
  }
}

export default App;

import got from "../data";

function searchByletter(letter) {
  let array = [];
  got.houses.forEach((element) => {
    element.people.forEach((item) => {
      if (item.name.toLowerCase().includes(letter.toLowerCase())) {
        array.push(item);
      }
    });
  });
  return array;
}

export default searchByletter;
